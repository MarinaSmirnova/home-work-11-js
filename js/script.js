const passwordIcons = document.querySelectorAll(".icon-password");

for (const icon of passwordIcons) {
    icon.addEventListener( "click" , function() {
        if (icon.classList.contains("fa-eye")) {
            changeInput(icon.dataset.area, "text")

        } else if (icon.classList.contains("fa-eye-slash")) {
            changeInput(icon.dataset.area, "password")
        }

        function changeInput(area, toInputType) {

            icon.classList.toggle("fa-eye");
            icon.classList.toggle("fa-eye-slash");

            const newInput = document.createElement("input");
            newInput.setAttribute("type", toInputType);
            newInput.setAttribute("data-area", area);

            const oldInput = document.querySelector('input[data-area=' + area + ']');
            newInput.value = oldInput.value;
            oldInput.replaceWith(newInput);
        }
        });
    }

    btn.addEventListener("click", function () {
        errorMsg.textContent = "";
        let enterPassword = document.querySelector('input[data-area="enterPassword"]');
        let checkPassword = document.querySelector('input[data-area="checkPassword"]');
        if ((enterPassword.value == false) || (checkPassword.value == false)) {
            showError("Введіть значення");
        } else {
            if (enterPassword.value === checkPassword.value) {
                setTimeout(() => {
                    alert("You are welcome");
                  }, 100);
            }
            else {
                showError("Потрібно ввести однакові значення");
            }
        }
        function showError(text) {
            errorMsg.innerHTML = text;
        }       
    })


